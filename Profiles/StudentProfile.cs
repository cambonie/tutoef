using AutoMapper;
using ContosoUniversityAPI.Models;

namespace ContosoUniversityAPI.Profiles
{
    public class StudentProfile : Profile
    {
        public StudentProfile()
        {
            CreateMap<Student, StudentDTO>();
        }
    }
}
