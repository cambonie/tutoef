using System;

namespace ContosoUniversityAPI.Models
{
    public class EnrollementDateGroupDTO
    {

        public DateTime? EnrollmentDate { get; set; }

        public int StudentCount { get; set; }
    }
}
