using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ContosoUniversityAPI.Data;
using ContosoUniversityAPI.Models;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;


namespace ContosoUniversityAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentController : ControllerBase
    {
        private readonly SchoolContext _context;
        private readonly IMapper _mapper;

        public StudentController(SchoolContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/student
        [HttpGet]
        public async Task<ActionResult<IEnumerable<StudentDTO>>> GetStudents(string? sort, string? search)
        {
            //LINQ
            var students = from s in _context.Students select s;

            if (search is { })
                students = students.Where(s => s.FirstName.ToLower().Contains(search.ToLower()) || s.LastName.ToLower().Contains(search.ToLower()));

            if (!students.Any()) return Ok();

            switch (sort)
            {
                case "date" :
                    students = students.OrderBy(s => s.EnrollmentDate);
                    break;

                case "date_desc":
                    students = students.OrderByDescending(s => s.EnrollmentDate);
                    break;

                case "name_desc":
                    students = students.OrderByDescending(s => s.LastName);
                    break;

                case "name" :
                default:
                    students = students.OrderBy(s => s.LastName);
                    break;
            }

            return Ok(await students.Select(student => _mapper.Map<StudentDTO>(student)).AsNoTracking().ToListAsync());
        }

        // GET: api/student/{id}
        [HttpGet("{id:int:min(1)}")]
        public async Task<ActionResult<StudentDTO>> Details(int id)
        {
            var studentDetails = await _context.Students
                .Include(student => student.Enrollments)
                .ThenInclude(enrollment => enrollment.Course )
                .AsNoTracking()
                .FirstOrDefaultAsync(s => s.ID == id);
            return (studentDetails is null) ? NotFound() : (ActionResult<StudentDTO>) Ok(_mapper.Map<StudentDTO>(studentDetails));
        }

        // POST: api/student
        [HttpPost]
        public async Task<ActionResult<Student>> PostStudent(Student student)
        {
            _context.Students.Add(student);
            await _context.SaveChangesAsync();
            return CreatedAtAction(nameof(Details), new { id = student.ID }, student);
        }

        // PUT: api/student
        [HttpPut("{id:int:min(1)}")]
        public async Task<ActionResult> EditStudent(int id, Student student)
        {
            if (id != student.ID) return BadRequest();

            _context.Entry(student).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (! _context.Students.Any(s => s.ID == id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }


        [HttpDelete("{id:int:min(1)}")]
        public async Task<ActionResult> DeleteStudent(int id)
        {
            var student = await _context.Students.FindAsync(id);

            if (student is null) return NotFound();

            _context.Students.Remove(student);
            await _context.SaveChangesAsync();
            return NotFound();
        }
    }
}




