using System.Data;
using System.Linq;
using System.Threading.Tasks;
using ContosoUniversityAPI.Data;
using ContosoUniversityAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ContosoUniversityAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AboutController : ControllerBase
    {
        private readonly SchoolContext _context;

        public AboutController(SchoolContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<IActionResult> About()
        {
            var data = from student in _context.Students
                group student by student.EnrollmentDate
                into dateGroup
                select new EnrollementDateGroupDTO()
                {
                    EnrollmentDate = dateGroup.Key,
                    StudentCount = dateGroup.Count()
                };

            return Ok(await data.AsNoTracking().ToListAsync());
        }
    }
}
